provider "aws" {
  region     = "us-east-1"
  access_key = ""
  secret_key = ""
 }


module "ec2_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name = "single-instance"

  ami                    = "ami-0ed9277fb7eb570c9"
  instance_type          = "t2.micro"
  
}